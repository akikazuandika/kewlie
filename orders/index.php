<?php

session_start();

if (!isset($_SESSION['email'])) {
    header("Location:login.php");
}

include '../Model.php';
include '../part/top.php';

$model = new Model();
$user = $model->getUser()->fetch_assoc();
$orders = $model->getUserOrders($user['id']);
?>


<!-- ****** Checkout Area Start ****** -->
<div class="checkout_area section_padding_100 shop">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
              <?php if ($orders->num_rows < 1): ?>
                <?php echo '<h2>Data empthy</h2>' ?>
              <?php else: ?>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <td>No</td>
                      <td>Title</td>
                      <td>Amount</td>
                      <td>Chair</td>
                      <td>Time</td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $n=1; while ($item = $orders->fetch_assoc()): ?>
                      <tr>
                        <td><?= $n++ ?></td>
                        <td><?= $item['productTitle'] ?></td>
                        <td><?= $item['orderAmount'] ?></td>
                        <td><?= $item['orderChair'] ?></td>
                        <td><?= date('d-m-Y', strtotime($item['productDate'])) . " " . date('H:i', strtotime($item['productTime'])) ?></td>
                      </tr>
                    <?php endwhile ?>
                  </tbody>
                </table>
              <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!-- ****** Checkout Area End ****** -->

<?php

include '../part/bottom.php';
?>
