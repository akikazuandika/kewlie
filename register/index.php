<?php

include '../Model.php';
$model = new Model();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Online Tickets Order | Home</title>

    <!-- Favicon  -->
    <link rel="icon" href="/assets/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="/assets/css/core-style.css">
    <link rel="stylesheet" href="/assets/style.css">

    <!-- Responsive CSS -->
    <link href="/assets/css/responsive.css" rel="stylesheet">

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="/assets/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Bootstrap js -->
    <script src="/assets/js/bootstrap.min.js"></script>
</head>

<body>
    <div id="wrapper">

        <!-- ****** Header Area Start ****** -->
        <header class="header_area">
            <!-- Top Header Area Start -->
            <div class="top_header_area">
                <div class="container h-100">
                    <div class="row h-100 align-items-center justify-content-end">

                        <div class="col-12 col-lg-7">
                            <div class="top_single_area d-flex align-items-center">
                                <!-- Logo Area -->
                                <div class="top_logo">
                                    <a href="/"><img style="width:168px;height:50px;" src="/assets/img/core-img/logo1.png" alt=""></a>
                                </div>
                                <!-- Cart & Menu Area -->
                                <div class="header-cart-menu d-flex align-items-center ml-auto">
                                    <!-- Cart Area -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>
        <!-- ****** Header Area End ****** -->

<!-- ****** Checkout Area Start ****** -->
<div class="checkout_area section_padding_100 shop">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6">
                <div class="checkout_details_area mt-50 clearfix">

                    <div class="cart-page-heading">
                        <h5>Sign Up</h5>
                    </div>

                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-12 mb-3">
                                <label for="first_name">Name <span>*</span></label>
                                <input type="text" class="form-control" id="first_name" name="name" required>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="street_address">Address <span>*</span></label>
                                <input type="text" class="form-control mb-3" id="street_address" name="address">
                            </div>
                            <div class="col-12 mb-3">
                                <label for="phone_number">Phone No <span>*</span></label>
                                <input type="number" class="form-control" id="phone_number" name="phone" >
                            </div>
                            <div class="col-12 mb-4">
                                <label for="email_address">Email Address <span>*</span></label>
                                <input type="email" class="form-control" id="email_address" name="email">
                            </div>
                            <div class="col-12 mb-3">
                                <label for="newPass">Password <span>*</span></label>
                                <input type="password" class="form-control" id="newPass" name="newPass">
                            </div>

                            <input type="submit" name="btnSubmit"  class="btn karl-checkout-btn" value="Sign Up">
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- ****** Checkout Area End ****** -->


<?php

include '../part/bottom.php';

if (isset($_POST['btnSubmit'])) {
  $name = $_POST['name'];
  $address = $_POST['address'];
  $phone = $_POST['phone'];
  $email = $_POST['email'];

  $conn = $model->conn();
  $hashedPass = password_hash( $_POST['newPass'], PASSWORD_BCRYPT );

  $query = " INSERT INTO users(name, address, no_hp, email, password) VALUES('$name', '$address', '$phone', '$email', '$hashedPass')";
  $exec = $conn->query($query);
  if ($exec == true) {
      echo "<script>alert('Success register'); window.location.href = '/';</script>";
  }else{
      echo "<script>alert('Failed register')</script>";
  }
}

?>
