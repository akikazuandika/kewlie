<?php

session_start();

if (!isset($_SESSION['email'])) {
    header("Location:login.php");
}

include '../Model.php';
include '../part/top.php';

$id = $_POST['productId'];

$model = new Model();
$product = $model->getProductById($id)->fetch_assoc();
$user = $model->getUser()->fetch_assoc();

?>


<!-- ****** Checkout Area Start ****** -->
<div class="checkout_area section_padding_100 shop">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6">
                <div class="checkout_details_area mt-50 clearfix">

                    <div class="cart-page-heading">
                        <h5>Billing Address</h5>
                    </div>

                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-12 mb-3">
                                <label for="first_name">Name <span>*</span></label>
                                <input type="text" class="form-control" id="first_name" value="<?= $user['name'] ?>" required>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="street_address">Address <span>*</span></label>
                                <input type="text" class="form-control mb-3" id="street_address" value="<?= $user['address'] ?>">
                            </div>
                            <div class="col-12 mb-3">
                                <label for="phone_number">Phone No <span>*</span></label>
                                <input type="number" class="form-control" id="phone_number" min="0" value="<?= $user['no_hp'] ?>">
                            </div>
                            <div class="col-12 mb-4">
                                <label for="email_address">Email Address <span>*</span></label>
                                <input type="email" class="form-control" id="email_address"  value="<?= $user['email'] ?>">
                            </div>
                            <div class="col-12 mb-4">
                                <label for="tickets">The number of tickets <span>*</span></label>
                                <input type="number" class="form-control" id="tickets" value="1" name="amount" max="16" min="1">
                            </div>

                            <input hidden type="text" class="form-control" id="tickets" value="<?= $product['id'] ?>" name="productId">
                            <input hidden type="text" class="form-control" id="tickets" value="<?= $user['id'] ?>" name="userId">
                        </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                <div class="order-details-confirmation">

                    <div class="cart-page-heading">
                        <h5>Your Order</h5>
                        <p>The Details</p>
                    </div>

                    <ul class="order-details-form mb-4">
                        <li><span>Product</span> <span>Total</span></li>
                        <li><span><?= $product['title']?></span> <span id="price" ><?= $product['price']?> </span></li>
                        <li><span>Amount</span> <span id="amount">0</span></li>
                        <li><span>Total</span> <span id="total">0</span></li>
                    </ul>


                    <div id="accordion" role="tablist" class="mb-4">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h6 class="mb-0">
                                    <input type="radio" name="payment" value="2">
                                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">On The Spot</a>
                                </h6>
                            </div>

                            <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Pay your tickets on the spot</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingThree">
                                <h6 class="mb-0">
                                    <input type="radio" name="payment" value="2">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Credit card</a>
                                </h6>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Pay with your credit card</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingFour">
                                <h6 class="mb-0">
                                  <input type="radio" name="payment" value="2">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">Direct bank transfer</a>
                                </h6>
                            </div>
                            <div id="collapseFour" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Manual bank transfer.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="submit" name="btnSubmit"  class="btn karl-checkout-btn" value="Place Order">
                    <!-- <a href="#" class="btn karl-checkout-btn">Place Order</a> -->
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ****** Checkout Area End ****** -->

<script>
  var amount = 1;
  var price = $("#price").html();
  var total = 0;

  total = amount * price

  $("#amount").html(amount)
  $("#tickets").val(amount)
  $("#total").html(total)

  $("#tickets").change(function() {
    amount = $("#tickets").val();
    total = amount * price

    $("#amount").html(amount)
    $("#tickets").val(amount)
    $("#total").html(total)
  })

</script>


<?php

include '../part/bottom.php';

if (isset($_POST['btnSubmit'])) {
  function generateRandomString($length = 1) {
      $characters = 'ABCDEFGHJKLMNOPQRST';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }


  $num = 0;
  $chair = '';

  if ($_POST['amount'] == 16) {
      $str = generateRandomString();
      $chair = $chair = $str . 1 . " - " . $str . 16;
  }else if ($_POST['amount'] > 1) {
      $max = 16 - $_POST['amount'];
      $num = (rand(1, $max));

      if ($num < 1 || $num > 16) {
        $num = (rand(1, $max));
      }
      $str = generateRandomString();
      $chair = $chair = $str . (($num-$_POST['amount'])+1) . " - " . $str . $num;
  }else{
      $num = rand(1, 16);
      $chair = generateRandomString() . $num;
  }

  if ($model->order($_POST['userId'], $_POST['productId'], $_POST['amount'], $chair) == true) {
    echo "<script>
      alert('Order success');
      window.location.href = '/';
    </script>";
  }else{
    echo "<script>alert('Order failed')</script>";
  }
}

?>
