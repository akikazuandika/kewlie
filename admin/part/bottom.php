
    </div>
    <!-- /#wrapper -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/admin/assets/vendor/metisMenu/metisMenu.min.js"></script>

    <script src="/admin/assets/dist/js/bootstrap-datepicker.js"></script>

    <script src="/admin/assets/dist/js/bootstrap-timepicker.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/admin/assets/dist/js/sb-admin-2.js"></script>

    <script type="text/javascript">
      $('#datepicker').datepicker({
      format: 'dd-mm-yyyy'
      });
    </script>

    <script type="text/javascript">
            $('#timepicker').timepicker({
              showMeridian: false,
            });
    </script>
</body>

</html>
