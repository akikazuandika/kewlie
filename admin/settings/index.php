<?php
include '../part/top.php';
include '../autoload.php';
$model = new Model();
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Settings</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Change Password
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <form role="form" method="post" action="" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Current Password</label>
                                    <input type="password" name="oldPass" class="form-control">
                                </div>
                                <button type="submit" class="btn btn-primary" name="btnSubmit">Save</button>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" name="newPass" class="form-control">
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

<!-- /#page-wrapper -->
<?php
include '../part/bottom.php';

$db = new Model();
$conn = $db->conn();
if (isset($_POST['btnSubmit'])) {
    $email = $_SESSION['email'];
    $password = $_POST['oldPass'];

    $query = "SELECT * FROM admins WHERE email='$email' ";
    $exec = $conn->query($query);
    $hashedPass = password_hash( $_POST['newPass'], PASSWORD_BCRYPT );

    if ($exec->num_rows > 0) {
        $user = $exec->fetch_assoc();
        if ( password_verify($password, $user['password']) ) {
            $query = "UPDATE admins SET password='$hashedPass' WHERE email='$email' ";
            $exec = $conn->query($query);
            if ($exec == true) {
                echo "<script>alert('Success change password')</script>";
            }else{
                echo "<script>alert('Failed change password')</script>";
            }
        }else{
            echo "<script>alert('Wrong password')</script>";
        }
    }else{
          echo "<script>alert('User not exists')</script>";
    }
}

?>
