<ul>
    <li>
        <a href="/admin">Home</a>
    </li>
    <li>
        <a href="/admin/products">Products</a>
    </li>
    <li>
        <a href="/admin/users">Users</a>
    </li>
    <li>
        <a href="/admin/orders">Orders</a>
    </li>
    <li>
        <a href="/admin/logout.php">Logout</a>
    </li>
</ul>