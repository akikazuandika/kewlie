<?php
include('../autoload.php'); 
$model = new Model();

if ($model->deleteProduct($_GET['id'])) {
    header("Location:index.php");
}else{
    header("Location:index.php?message=delete-failed");
}

header("Location:index.php");