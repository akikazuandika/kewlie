<?php
include '../part/top.php';
include('../autoload.php');
$model = new Model();
$users = $model->getAllUsers();
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Users</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                          <tr>
                              <td>No</td>
                              <td>Name</td>
                              <td>Email</td>
                              <td>No HP</td>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n=1; while($item = $users->fetch_assoc()): ?>
                          <tr>
                              <td><?= $n++ ?></td>
                              <td><?= $item['name'] ?></td>
                              <td><?= $item['email'] ?></td>
                              <td><?= $item['hp'] ?></td>
                          </tr>
                          <?php endwhile ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->
<?php include '../part/bottom.php'; ?>
