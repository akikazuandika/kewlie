<?php
include '../part/top.php';
include('../autoload.php');
$model = new Model();
$users = $model->getAllOrders();
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Orders</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                          <tr>
                              <td>No</td>
                              <td>Name</td>
                              <td>No HP</td>
                              <td>Title</td>
                              <td>Time</td>
                              <td>Amount</td>
                              <td>Chair</td>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n=1; while($item = $users->fetch_assoc()): ?>
                          <tr>
                              <td><?= $n++ ?></td>
                              <td><?= $item['userName'] ?></td>
                              <td><?= $item['userHp'] ?></td>
                              <td><?= $item['productTitle'] ?></td>
                              <td><?= date('d-m-Y', strtotime($item['productDate'])) . " " . date('H:i', strtotime($item['productTime'])) ?></td>
                              <td><?= $item['orderAmount'] ?></td>
                              <td><?= $item['orderChair'] ?></td>
                          </tr>
                          <?php endwhile ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->
<?php include '../part/bottom.php'; ?>
